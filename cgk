#!/usr/bin/python3

import argparse
import getpass
import os
import tempfile
import socket
import sys
import re
import time
import io
import pycurl

import subprocess

import gssapi


def getdcname(kdcname, verbose):
    try:
        srvip = socket.gethostbyname(kdcname)
        try:
            srvname = socket.gethostbyaddr(srvip)[0]
            if verbose:
                print("Found KDC: %s" % srvname)
            return srvname
        except:
            errormsg('Cannot find Domain Controller name ("' + kdcname + ' - ' + srvip + '") in DNS')
    except:
        errormsg('Cannot find Domain Controller IP ("' + kdcname + '") in DNS')

def print_verbose(msg, verbose):
    if verbose:
        print(msg)

def print_debug(msg, debug):
    if debug:
        print(msg)

def do_execute (cmd, verbose, print_output=True, can_fail=False, return_output=False, hide_password=True):
    # Only --user passes --use-service-account
    if re.search(r'use-service-account', cmd) is not None:
        ommitted_password = f"{re.search(r'^.*old-account-password', cmd).group(0)} *********"
    else:
        ommitted_password = cmd
    if print_output:
        if verbose:
            if hide_password:
                print(f"running: {ommitted_password}")
            else:
                print(f"running: {cmd}")
    try:
        p = subprocess.Popen(cmd, stderr=subprocess.PIPE, shell=True, stdout=subprocess.PIPE)
    except:
        if hide_password:
            print(f"Failed to execute: {ommitted_password}")
        else:
            print(f"Failed to execute: {cmd}")
        sys.exit(1)

    out, err = p.communicate()
    if len(err) > 0 and print_output:
        print(err.decode().strip())
    if p.wait() != 0:
       if can_fail:
           return False
       print('Error executing command.')
       sys.exit(p.wait())
    if verbose:
      if len(out) > 0 and print_output:
          print(out.decode().strip())
    if return_output:
        return out.decode().strip()
    return True

def fixselinux(keytab, verbose):
    chcon = '/usr/bin/chcon'
    try:
        devicenum = os.stat(keytab).st_dev
    except:
        return
    if (devicenum > 0 and devicenum != 43) and (os.path.exists('/sys/fs/selinux') or os.path.exists('/selinux/status')) and os.access(chcon, os.R_OK):
        do_execute(f"{chcon} system_u:object_r:krb5_keytab_t:s0 {keytab}", verbose)

def get_enctypes(enctypes):
    allowedenctypes = { 'DES_CBC_CRC': 1, 'DES_CBC_MD5': 2, 'RC4_HMAC_MD5': 4, 'AES128_CTS_HMAC_SHA1': 8, 'AES256_CTS_HMAC_SHA1': 16}
    encs = 0
    if '|' in enctypes:
        for e in enctypes.split('|'):
            if e in allowedenctypes:
                encs += allowedenctypes[e]
            else:
                errormsg("Error: Unknown enctype specified. Allowed ones are: DES_CBC_CRC|DES_CBC_MD5|RC4_HMAC_MD5|AES128_CTS_HMAC_SHA1|AES256_CTS_HMAC_SHA1")
    else:
        try:
            encs = allowedenctypes[enctypes]
        except:
            errormsg("Error: Unknown enctype specified. Allowed ones are: DES_CBC_CRC|DES_CBC_MD5|RC4_HMAC_MD5|AES128_CTS_HMAC_SHA1|AES256_CTS_HMAC_SHA1")

    return encs

def call_msktutil(ccache, keytab, kdcserver, principal, hostname, service, enctypes, isolate, userkeytab, userpass, userlogin, alias, cname, remove, verbose, debug, password=False, smbpassword=False):
    d = dict(os.environ)
    d['KRB5CCNAME'] = f"FILE:{ccache}"

    if not service:
        service = 'host'

    if alias or cname:
        cmd = "/usr/sbin/msktutil --update --dont-expire-password --no-canonical-name --dont-update-dnshostname --hostname"
        if alias:
            cmd = f"{cmd} {alias}"
        else:
            cmd = f"{cmd} {cname}"
    else:
        cmd = "/usr/sbin/msktutil --update --dont-expire-password"
    if userkeytab:
        cmd = f"{cmd} --base OU=Users"
    elif alias: 
        cmd = f"{cmd} --base 'DC=cern,DC=ch'"
    else:
        cmd = f"{cmd} --base OU=Computers"

    if verbose:
        cmd = f"{cmd} --verbose"
    if debug:
        # Yes, this is not a typo
        cmd = f"{cmd} --verbose --verbose"

    if isolate:
        # this option is available on patched msktutil-1.0 only ..
        cmd = f"{cmd} --dont-update-dnshostname"

    if remove:
        cmd = f"{cmd} --remove-service {remove}"
    else:
        if not userkeytab and service:
            cmd = f"{cmd} --service {service}"

    if hostname:
        if 'dyndns.cern.ch' in hostname:
            cmd = f"{cmd} --nocanonical-name --hostname {hostname}.cern.ch"
        else:
            cmd = f"{cmd} --hostname {hostname}"
        
    if keytab:
        cmd = f"{cmd} --keytab {keytab}"
    if kdcserver:
        cmd = f"{cmd} --server {kdcserver}"
    if enctypes:
        cmd = f"{cmd} --enctypes {enctypes}"
    if smbpassword:
        cmd = f"{cmd} --set-samba-secret"
    if userkeytab:
        cmd = f"{cmd} --dont-change-password --use-service-account"
    if userlogin:
        cmd = f"{cmd} --account-name {userlogin}"
    if userpass:
        cmd = f"{cmd} --old-account-password {userpass}"
    if principal:
        cmd = f"{cmd} --computer-name {principal.replace('$','')}"
    # we've had a reset pass call
    if password:
        cmd = f"{cmd} --old-account-password {password}"
       
    if do_execute(cmd, verbose):
        fixselinux(keytab, verbose)
        print(f"Keytab file saved: {keytab}")

def krb5cfgfile(kdcserver, krealm, leavekrb5cfg, verbose, debug):
    if leavekrb5cfg is False:
        tfname = generate_temp_file('cgk.krb5.conf.')
    else:
        tfname = '/tmp/cgk.krb5.conf'
    try:
        tfh = open(tfname, 'w')
    except FileNotFoundError:
        errormsg(f"cannot create temporary krb5 config file: {tfname}")
    try:
        newline = '\n'
        tfh.writelines('[libdefaults]\ndefault_realm=' + krealm + '\ndns_lookup_kdc=false\nforwardable=true\nproxiable=true\n[realms]\n' + krealm + '={\nkdc=' + kdcserver + '\nadmin_server=' + kdcserver + '\n}\n')
        print_debug(f"created temporary krb5 config file: {tfname}", debug)
    except Exception as e:
        errormsg(f"cannot write temp. krb5 config file: {tfname} [{e}]")

def errormsg(msg):
    print(f"Error: {msg}")
    sys.exit(1)

def generate_temp_file(prefix):
    try:
      tfh = tempfile.NamedTemporaryFile(mode='w', prefix=prefix, dir='/tmp')
      filename = tfh.name
    except:
      errormsg(f"cannot create temporary {prefix} config file : {filename}")
    return filename

def kinit(principal,keytab,ccache,verbose):
    # surely this can be done via gssapi? (not sure actually ...)
    cmd = f"/usr/bin/kinit -k -t {keytab} -c {ccache} {principal}"
    if do_execute(cmd, verbose, print_output=False, can_fail=True):
        return True
    else:
        return False

def reset_password(verbose, debug, server, verify_peer, verify_host, alias=None, cname=None, service=None, isolate=None):
    if alias:
        KRB_KEYTAB_RESOURCE = 'ResetComputerPasswordTimeCheckDnsAlias'
    elif cname:
        KRB_KEYTAB_RESOURCE = 'ResetComputerPasswordTimeCheckCName'
    else:
        KRB_KEYTAB_RESOURCE = 'ResetComputerPasswordTimeCheck'

    if not isolate or not service:
        service = 'host'

    url = f"{server}/{KRB_KEYTAB_RESOURCE}?service={service}"
    if alias:
        url = f"{url}&dnsalias={alias}"
    if cname:
        url = f"{url}&cname={cname}"
    reqtimestamp = time.strftime("%FT%T", time.gmtime())
    url = f"{url}&t={reqtimestamp}"

    buffer = io.BytesIO()
    errorbuffer = io.BytesIO()
    # pycurl used in lieu of requests to ensure the same behaviour as the perl port
    c = pycurl.Curl()
    c.setopt(pycurl.LOCALPORT, 600)
    c.setopt(pycurl.LOCALPORTRANGE, 100)
    c.setopt(pycurl.USERAGENT, 'cern-get-keytab/1.2.0' )
    c.setopt(pycurl.FOLLOWLOCATION, 1)
    c.setopt(pycurl.CONNECTTIMEOUT, 10)
    c.setopt(pycurl.TIMEOUT, 60)
    if verify_peer:
        verify_peer = 1
    else:
        verify_peer = 0
    c.setopt(pycurl.SSL_VERIFYPEER, verify_peer )
    if verify_host:
        verify_host = 2
    else:
        verify_host = 0
    c.setopt(pycurl.SSL_VERIFYHOST, verify_host )

    c.setopt(c.WRITEDATA, buffer)

    if 'HTTPS_PROXY' in os.environ.keys() or 'https_proxy' in os.environ.keys():
        krbhost = re.search(r'(?<=://)[\w-]+(?:\.[\w-]+)+\b', url).group(0)
        print_verbose(f"https proxy detected, instructing call to {krbhost} to ignore the configured proxy", args.verbose)
        c.setopt(pycurl.NOPROXY, krbhost)

    c.setopt(pycurl.URL, url.encode())

    print_verbose(f"curl GET {url}", verbose)
    try:
        c.perform()
    except pycurl.error:
        errormsg(f"cannot reset host password [http err: {c.getinfo(pycurl.HTTP_CODE)} ])")
    data = buffer.getvalue()
    if c.getinfo(pycurl.HTTP_CODE) != 200:
        newline='\n'
        errormsg(f"server error: {c.getinfo(pycurl.HTTP_CODE)}, server output:{newline}{data}")

    results = {}
    # krbwin returns XML, but it seems to be broken and doesn't parse with
    # the python xml library
    for line in data.decode().split('\n'):
        for match in [ 'error', 'success', 'principalname', 'hostname', 'computerpassword', 'samaccountname' ]:
            regex = r'(?<=' + re.escape(match) + '>)(.*)(?=<\/' + re.escape(match) + ')'
            m = re.search(regex, line)
            if m is not None:
                results[match] = m.group(0)

    print_verbose("curl RCV data:", verbose)
    print_verbose(f"success code: {results['success']}", verbose)

    if 'true' in results['success']:
        print_verbose(f"computer pass: {results['computerpassword']}", verbose)
        print_verbose(f"samaccountname: {results['samaccountname']}", verbose)
        print_verbose(f"principalname: {results['principalname']}", verbose)
    else:
        print_verbose(f"error string: {results['error']}", verbose)
        errormsg(f"cannot receive/parse server data:{results['error']}")

    if 'computerpassword' not in results.keys() or 'samaccountname' not in results.keys():
        errormsg("incomplete data received.")

    return results['samaccountname'], results['computerpassword']

def check_alpha_numeric(value):
    m = re.search(r'^[a-zA-Z0-9_-]+$', value)
    if m is not None:
        return
    else:
        errormsg("remove service name can contain only alphanumeric characters ([a-zA-Z0-9_-]).")

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--keytab', required=False, type=str, action='store', default='/etc/krb5.keytab', help='Stores retrieved keytab in FILENAME rather than default krb5.keytab')
    parser.add_argument('--force', required=False, action='store_true', help='Appends new to already existing valid host/service keytab file, invalidating current entries.')
    parser.add_argument('--service', required=False, type=str, action='store', help='Acquires keytab for given service rather than host. SRVC name is case-sensitive (example common service names: HTTP, cvs, ...). See also --isolate option. If not specified the default service: host is used.')
    parser.add_argument('--remove', required=False, type=check_alpha_numeric, action='store', help='Removes keys for named service from Active Directory')
    parser.add_argument('--isolate', required=False, action='store_true', help='Active Directory provides keytabs for host and services using same encryption keys. This option allows for creating service keytab with encyption keys different that host keytab encryption keys for improved security. By default such keytabs are stored as /etc/krb5.keytab.SRVC unless --keytab option is specified.')
    parser.add_argument('--hostname', required=False, type=str, action='store', help='Use HOSTNAME for requesting Active Directory password reset. This option is to be used ONLY when system has multiple interfaces / ip addresses allocated and keytab is required for hostname corresponding to a non-default route interface.  System network interface corresponding to HOSTNAME must be defined and active before running cern-get-keytab with this option specified.  This option will not funciton while using DNS host aliases. See next option.')
    parser.add_argument('--alias', required=False, type=str, action='store', help='Acquire keytab for CERN DNS Dynamic Alias (delegated zone/subdomain), or Landb load balancing alias (*--load-X-) This operation will only succeed if executed on a system being active member of DNS alias.')
    parser.add_argument('--cname', required=False, type=str, action='store', help='Acquire keytab for CERN DNS Dynamic Alias (delegated zone/subdomain), or Landb load balancing alias (*--load-X-) This operation will only succeed if executed on a system being active member of DNS alias.')
    parser.add_argument('--user', required=False, action='store_true', help='Generate keytab for user account, this option requires specifying user account password and optionally user login.  Such keytab can be used for authentication with kinit: # kinit -kt /path/to/user.keytab.file login WARNING: store user keytab in protected location: this file contains your user credentials which can be used to authenticate until account password is changed.')
    parser.add_argument('--password', required=False, type=str, action='store', help='User account password for user keytab generation. If not provided, will be asked interactively. Special characters in password need to be shell-escaped.')
    parser.add_argument('--login', required=False, type=str, action='store', help='User account login for user keytab generation. If not provided current username is used.')
    parser.add_argument('--enctypes', required=False, type=str, action='store', help='Specify encryption types to be used for obtained host identity.  Allowed (and supported on CERN Active Directory) ENCTYPEs are: DES_CBC_CRC DES_CBC_MD5 RC4_HMAC_MD5 (RC4) AES128_CTS_HMAC_SHA1 (AES128) AES256_CTS_HMAC_SHA1 (AES256) Default ENCTYPEs are: RC4_HMAC_MD5|AES128_CTS_HMAC_SHA1|AES256_CTS_HMAC_SHA1')
    parser.add_argument('--passwordsmb', required=False, action='store_true', help='Saves computer account password in Samba secrets database (/var/lib/samba/private/secrets.tdb) for use with kerberized smbd setup.')
    parser.add_argument('--leavekrb5cfg', required=False, action='store_true', default=False, help='Store used temporary kerberos config file as: /tmp/cgk.krb5.conf This can be used by another tool in order to guarantee that same Active Directory Domain Controller will be used, therefore eliminating the need to wait for (asynchronous) AD replication.  To use from (ba)sh shell: export KRB5_CONFIG=/tmp/cgk.krb5.conf Warning: if /tmp/cgk.krb5.conf exists its content will be overwritten.  ')
    parser.add_argument('--testsrv', required=False, action='store_true', default=False, help='Use development instance of password reset server. DO NOT USE IN PRODUCTION.')
    parser.add_argument('--verbose', required=False, action='store_true', help='Display verbose information')
    parser.add_argument('--debug', required=False, action='store_true', help='Display debug information')
    args = parser.parse_args()

    if os.geteuid() != 0 and not args.user:
        errormsg("You must be 'root' to run this program.")
    if args.testsrv:
        print("--testsrv is deprecated. Please use a configuration file instead.")

    # set defaults, these may be overridden if the user supplied a config file
    verify_peer=True
    verify_host=True
    if args.testsrv:
        server = 'https://lxkerbwindev.cern.ch/LxKerb.asmx'
    else:
        server = 'https://lxkerbwin.cern.ch/LxKerb.asmx'

    if 'CERN_GET_KEYTAB_CONF' in os.environ.keys():
        config_file = os.environ['CERN_GET_KEYTAB_CONF']
    else:
        config_file = '/etc/cern-get-keytab.yaml'
    try:
        cfh = open(config_file, 'r')
        for line in cfh.readlines():
            if '#' not in line.strip()[0]:
                l = line.strip().split(': ')
                if len(l) == 2:
                    if 'server' in l[0]:
                        server = l[1]
                    if 'verify_peer' in l[0]:
                        verify_peer = l[1]
                    if 'verify_host' in l[0]:
                        verify_host = l[1]
    # Fix this exception
    except:
        pass
    if args.isolate and not args.service:
        errormsg("--isolate option must be used together with --service option.")

    if args.service and args.remove:
        errormsg("only one of --service or --remove can be used at the same time.")
    if args.enctypes:
        args.enctypes = get_enctypes(args.enctypes)

    kdcserver = getdcname ('cerndc.cern.ch', args.verbose)
    krealm = 'CERN.CH'

    if args.passwordsmb:
        do_execute('/usr/libexec/cern-get-keytab-samba-workaround', args.verbose, print_output=True, can_fail=True)

    print_verbose("Setting up temporary Kerberos config file", args.verbose)
    krb5cfgfile(kdcserver, krealm, args.leavekrb5cfg, args.verbose, args.debug)
    # This is not needed in this python port, however keeping this
    # to have the same output as the perl version
    print_verbose("Initializing Kerberos client", args.verbose)

    if not args.user:
        print_verbose("Authenticating using keytab file.", args.verbose)

    if args.keytab == '/etc/krb5.keytab':
        if args.user:
            errormsg("--user option must be used together with --keytab option.")
        if args.isolate:
            args.keytab = f"/etc/krb5.keytab.{args.service}"
            print_verbose(f"using {args.keytab} as keytab file", args.verbose)
        else:
            print_verbose("using default keytab file name.", args.verbose)


    if args.user:
        if not args.login:
            args.login = os.getlogin()
        if not args.password:
            args.password = getpass.getpass(prompt=f"Password for {args.login}: ")
        call_msktutil(None, args.keytab, kdcserver, None, args.hostname, args.service, args.enctypes, args.isolate, args.user, args.password, args.login, args.alias, args.cname, args.remove, args.verbose, args.debug, None, args.passwordsmb)
        sys.exit(0)

    ccache_result = False
    print_verbose(f"using keytab file name: {args.keytab}", args.verbose)
    if not os.access(args.keytab, os.R_OK):
        print_verbose("keytab file not readable.", args.verbose)
    # OMG, we have a keytab
    else:
        ccache_file = generate_temp_file('cgk.ccache.')
        print_verbose(f"resolving credentials cache ({ccache_file}).", args.verbose)
        print_verbose(f"resolving keytab file {args.keytab}", args.verbose)
        print_verbose("scanning keytab file for principal name", args.verbose)
        principal = True
        if not do_execute(f"/usr/bin/klist -k {args.keytab}", args.verbose, print_output=False, can_fail=True):
            print(f"{args.keytab} is corrupt, removing")
            try:
                os.unlink(args.keytab)
            except:
                pass
            principal = False
        if principal:
            # finally some gssapi code (or not)
            # There really should be a way to inspect the keytab for principals ...
            klist = do_execute(f"/usr/bin/klist -k {args.keytab}", args.verbose, return_output=True, print_output=False, can_fail=True)
            for line in klist.splitlines():
                if re.search(r'^.*\$@CERN\.CH', line):
                    principal = line.split()[1].split('@')[0]
                    break
            print_verbose(f"found principal name: {principal}", args.verbose)
            print_verbose(f"authenticating as: {principal} using keytab file: {args.keytab}.", args.verbose)
            #This is done in MEMORY: on the perl port, not sure if we can do that here ...
            ccache_result = kinit(principal, args.keytab, ccache_file, args.verbose)
        else:
            print("principal name not found")
    if not ccache_result:
        print_verbose("Authentication using keytab file failed", args.verbose)
        print_verbose("Requesting password reset", args.verbose)
        principal, password = reset_password(args.verbose, args.debug, server, verify_peer, verify_host, args.alias, args.cname, args.service, args.isolate)
        time_elapsed = 0
        password_auth = False
        ccache_file = generate_temp_file('cgk.ccache.')
        while time_elapsed < 60:
            print(f"Waiting for password replication ({time_elapsed} seconds past)")
            print_verbose(f"authenticating as {principal} using password ({password}).", args.verbose)
            # gssapi call (finally)
            store = {'ccache': ccache_file}
            name = gssapi.Name(principal, name_type=gssapi.NameType.user)
            try:
                print_verbose(f"Authenticating using password (try {int((time_elapsed+5)/5)}/10).", args.verbose)
                res = gssapi.raw.acquire_cred_with_password(name, password.encode())
                creds = gssapi.Credentials(res.creds)
                creds.store(store=store, overwrite=True)
                password_auth = True
                break
            except gssapi.raw.misc.GSSError:
                pass
            time.sleep(5)
            time_elapsed += 5
        if password_auth:
            print_verbose("Successfully authenticated using password", args.verbose)
            call_msktutil(ccache_file, args.keytab, kdcserver, principal, args.hostname, args.service, args.enctypes, args.isolate, args.user, args.password, args.login, args.alias, args.cname, args.remove, args.verbose, args.debug, password, args.passwordsmb)
        else:
            print("Authentication using keytab file and password failed.")
            errormsg("Cannot get keytab")
    else:
        print_verbose("Successfully authenticated using keytab file", args.verbose)
        if args.force:
            call_msktutil(ccache_file, args.keytab, kdcserver, principal, args.hostname, args.service, args.enctypes, args.isolate, args.user, args.password, args.login, args.alias, args.cname, args.remove, args.verbose, args.debug, False, args.passwordsmb)
        else:
            print(f"Current keytab file ({args.keytab}) is valid.")
            print("Use --force to reinitialize.")

if __name__ == "__main__":
    main()
