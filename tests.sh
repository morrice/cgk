#!/bin/bash

function print_header {
echo "-------------------------------"
echo "$1"
echo "-------------------------------"
}

USER=notme
PASSWORD=notmypassword
VERBOSE="--verbose"
#VERBOSE=""
KEYTAB=/etc/krb5.keytab
SERVICE="HTTP"
CGK="./cgk"
#CGK="cern-get-keytab"

set -e

print_header "Executing with no parameters"
$CGK $VERBOSE
print_header "Forcing new keytab"
$CGK $VERBOSE --force
print_header "Removing keytab"
rm -f $KEYTAB
print_header "Forcing new keytab (with a password reset)"
$CGK $VERBOSE --force
print_header "Adding service $SERVICE to system keytab"
$CGK --service $SERVICE --force
print_header "Removing service $SERVICE from system keytab"
$CGK --remove $SERVICE --force
print_header "Adding service $SERVICE with --isolate"
$CGK --service $SERVICE --isolate --force
print_header "Creating user ($USER) keytab (/tmp/krb5.keytab.$USER)"
$CGK --user --login $USER --password $PASSWORD --keytab /tmp/krb5.keytab.$USER
